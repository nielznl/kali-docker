FROM kalilinux/kali-rolling

WORKDIR /volume
ARG DEBIAN_FRONTEND=noninteractive

RUN echo 'deb http://http.kali.org/kali kali-rolling main contrib non-free' > /etc/apt/sources.list
RUN apt -y update && apt -y install command-not-found
RUN apt -y update && apt -y dist-upgrade && apt -y autoremove && apt clean

# Usual packages
RUN apt -y install \
        man-db \
        curl \
        wget \
        net-tools \
        iputils-ping \
        bind9-dnsutils \
        whois \
        neovim

# Tools
RUN apt -y install \
        seclists \
        ffuf
